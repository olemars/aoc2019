# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  seq = [int(x) for x in raw]
  
  basepattern = [1,0,-1, 0]
  
  #part 1
  phases = 100
  phase = seq.copy()
  for i in range(phases):
    for v in range(len(phase) // 2):
      f = 0
      for w in range(v, len(phase)):
        p = ((w - v) // (v+1)) % 4
        f += phase[w] * basepattern[p]
      phase[v] = abs(f) % 10
    for v in range(len(phase) // 2, len(phase)):
      f = sum(phase[v:])
      phase[v] = abs(f) % 10
  print("part 1: ", "".join([str(n) for n in phase[:8]]))
  
  #part 2
  phases = 100
  rep = 10000
  offset = int(raw[:7])
  phase = seq.copy()
  seqlen = len(phase)
  replen = seqlen * rep
  adjoffset = offset - (offset % seqlen)
  arr = phase * ((replen-adjoffset) // seqlen)
  for i in range(phases):
    last = 0
    for v in range(replen - adjoffset - 1, -1, -1):      
      new = (arr[v] + last) % 10
      last = new
      arr[v] = new
  print("part 2: ", "".join([str(n) for n in arr[offset-adjoffset:offset-adjoffset+8]]))
  
  t1 = time.perf_counter()
  print(t1-t0)
  