# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer
from matplotlib import pyplot as plt
import numpy as np
from collections import deque

offset = 31
board = np.full((64, 64), fill_value=-1)

def setmaptile(coord, v):
  offset = 31
  tiles = {"0": 0, "1": 1, "2": 2, "3": 3}
  board[coord[0] + offset, coord[1] + offset] = tiles[v]
  
fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  puter = Intputer(code.copy(), False)
  exitcode = 0
  output = []
  loc = (0,0)
  mov = {(1, 0): 1, (-1, 0): 2, (0, -1): 3, (0, 1): 4}
  state = {loc:code.copy()}
  path = {loc: None}
  source = {loc: loc}
  targets = deque()
  board[loc[0] + offset, loc[1] + offset] = 3
  #im = plt.imshow(board)
  oxygen = None
  for k in mov.keys():
    t = (loc[0] + k[0], loc[1] + k[1])
    if t not in path:
      targets.appendleft(t)
      source[t] = loc
  while exitcode not in [-1, 2] and len(targets) > 0:
    target = targets.popleft()
    loc = source[target]
    d = target[0] - loc[0], target[1] - loc[1]
    heading = mov[d]
    path[target] = loc
    puter.program = state[path[target]].copy()
    output, exitcode = puter.run([heading], True)
    setmaptile(target, output[0])
    if output[0] != "0":
      loc = target  
      for k in mov.keys():
        t = (loc[0] + k[0], loc[1] + k[1])
        if t not in path:
          targets.appendleft(t)
          source[t] = loc
    if output[0] == "2":
      oxygen = target
    #im.set_data(board)
    #plt.pause(0.01)
    if loc not in state:
      state[loc] = puter.program.copy()
  #plt.imshow(board)
  
  steps = 0
  step = oxygen
  while path[step] is not None:
    steps += 1
    step = path[step]
  print("part 1 : ", steps)
  
  #part 2
  loc = oxygen
  targets = deque()
  path = {loc: None}
  source = {loc: loc}
  for k in mov.keys():
    t = (loc[0] + k[0], loc[1] + k[1])
    if t not in path:
      targets.appendleft(t)
      source[t] = loc
  while exitcode not in [-1, 2] and len(targets) > 0:
    target = targets.pop()
    loc = source[target]
    d = target[0] - loc[0], target[1] - loc[1]
    heading = mov[d]
    path[target] = loc
    puter.program = state[path[target]].copy()
    output, exitcode = puter.run([heading], True)
    if output[0] != "0":
      loc = target
      setmaptile(target, "3")
      for k in mov.keys():
        t = (loc[0] + k[0], loc[1] + k[1])
        if t not in path:
          targets.appendleft(t)
          source[t] = loc
    #im.set_data(board)
    #plt.pause(0.01)
    if loc not in state:
      state[loc] = puter.program.copy()
  #plt.imshow(board)
  
  minutes = 0
  step = loc
  while path[step] is not None:
    minutes += 1
    step = path[step]
  print("part 2 : ", minutes)
  
  t1 = time.perf_counter()
  print(t1-t0)