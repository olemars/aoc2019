# -*- coding: utf-8 -*-

from common import fetchinput    
import time    
import matplotlib.pyplot as plt

def createLine(p1, p2):
    x = (min(p1[0], p2[0]), max(p1[0], p2[0]))
    y = (min(p1[1], p2[1]), max(p1[1], p2[1]))
    return (x, y)
    
def intersect(l1, l2):
    if l1[0][0] == l1[0][1]: #l1 vertical
        if l1[0] != l2[0]: 
            if l1[1][0] <= l2[1][0] <= l1[1][1] and l2[0][0] <= l1[0][0] <= l2[0][1]:
                return (l1[0][0], l2[1][0])
    elif l1[1][0] == l1[1][1]: #l1 horiz
        if l1[1] != l2[1]:
            if l1[0][0] <= l2[0][0] <= l1[0][1] and l2[1][0] <= l1[1][0] <= l2[1][1]:
                return (l2[0][0], l1[1][0])
    return None
    
def calcWireLines(coords):
    lines = []
    for i in range(len(coords) - 1):
        lines.append(createLine(coords[i], coords[i+1]))
    return lines
    
def calcWireCoord(wire):
    coords = [(0,0)]
    for seg in wire:
        d = seg[0]
        l = int(seg[1:])
        if d == 'R':
            coords.append((coords[-1][0] + l, coords[-1][1]))
        elif d == 'L':
            coords.append((coords[-1][0] - l, coords[-1][1]))
        elif d == 'U':
            coords.append((coords[-1][0], coords[-1][1] + l))
        elif d == 'D':
            coords.append((coords[-1][0], coords[-1][1] - l))
    return coords

def calcWireSteps(wire):
    steps = [0]
    stepsum = 0
    for seg in wire:
        l = int(seg[1:])
        stepsum += l
        steps.append(stepsum)
    return steps


fetchinput()
with open("input.txt") as infile:
    t0 = time.perf_counter()
    raw = infile.read().strip()
    wires = [l.split(",") for l in raw.split("\n")]
    wirecoords = [calcWireCoord(wire) for wire in wires]
    wiresteps = [calcWireSteps(wire) for wire in wires]
    wirelines = [calcWireLines(coords) for coords in wirecoords]
    
    points = []
    dists = []
    steps = []
    for i in range(len(wirelines[0])):
        for j in range(len(wirelines[1])):
            point = intersect(wirelines[0][i], wirelines[1][j])
            if point is not None:
                points.append(point)
                dists.append(abs(point[0]) + abs(point[1]))
                partpoint1 = tuple(abs(a - b) for a, b in zip(point, wirecoords[0][i]))
                partpoint2 = tuple(abs(a - b) for a, b in zip(point, wirecoords[1][j]))
                steps.append(wiresteps[0][i] + sum(partpoint1) \
                             + wiresteps[1][j] + sum(partpoint2))
    
    #part 1
    print(min(dists))
    
    #part 2
    print(min(steps))
    t1 = time.perf_counter()
    print(t1 - t0)
    for coords in wirecoords:
        plt.plot(*zip(*coords))
    plt.scatter(*zip(*points))
    plt.show()