# -*- coding: utf-8 -*-
import time
from common import fetchinput

def validate(v, maxlen = 6):
  match = False
  last = v % 10
  for i in range(maxlen-1):
    v //= 10
    curr = v % 10
    if curr > last:
      return False
    match |= curr == last
    last = curr
  return match
  
def validate2(v, maxlen = 6):
  count = 1
  match = False
  last = v % 10
  for i in range(maxlen-1):
    v //= 10
    curr = v % 10
    if curr > last:
      return False
    if curr == last:
      count += 1
    else:
      match |= count == 2
      count = 1
    last = curr
    
  match |= count == 2
  return match
  
fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  minval, maxval = (int(v) for v in raw.split("-"))
  maxlen = 6
  
  #part 1
  total = 0
  for v in range(minval, maxval):
    total += validate(v)
  print("Part 1: ", total)
  
  #part 2
  total = 0
  for v in range(minval, maxval):
    total += validate2(v)
  print("Part 2: ", total)
    
  t1 = time.perf_counter()
  print(t1-t0)
  