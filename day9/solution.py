# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  puter = Intputer(code.copy())
  output, exitcode = puter.run(["1"])
  
  #part 2
  puter = Intputer(code.copy())
  output, exitcode = puter.run(["2"])
    
  t1 = time.perf_counter()
  print(t1-t0)
  