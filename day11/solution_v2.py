# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer
from collections import deque, defaultdict
from matplotlib import pyplot as plt

def painter(code, startcolor):
  directions = {"u": (0, 1), "r": (1, 0), "d": (0, -1), "l": (-1, 0)}
  curdir = deque(["u", "r", "d", "l"])
  curpos = (0,0)
  path = defaultdict(int, {(0, 0): int(startcolor)})
  exitcode = 0
  output = []
  input = startcolor
  puter = Intputer(code.copy(), False)
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run([input], True)
    if len(output) == 2:
      path[curpos] = int(output[0])
      curdir.rotate(1 - (2 * int(output[1])))
      curpos = (curpos[0] + directions[curdir[0]][0], curpos[1] + directions[curdir[0]][1])
      input = str(path[curpos])
  return path

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  path = painter(code, "0")
  print("part 1: ", len(path))
  
  #part 2
  path = painter(code, "1")
  plt.ylim(-20, 20)
  plt.scatter(*zip(*[k for k,v in path.items() if v == 1]))
  plt.show()
  
  t1 = time.perf_counter()
  print(t1-t0)
  