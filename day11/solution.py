# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer
from collections import deque
from matplotlib import pyplot as plt

def painter(code, startcolor):
  directions = {"u": (0, 1), "r": (1, 0), "d": (0, -1), "l": (-1, 0)}
  curdir = deque(["u", "r", "d", "l"])
  curpos = (0,0)
  white = set()
  black = set()
  path = [(0, 0)]
  exitcode = 0
  output = []
  input = startcolor
  puter = Intputer(code.copy(), False)
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run([input], True)
    if len(output) == 2:
      if output[0] == "0":
        black.add(curpos)
        if curpos in white:
          white.remove(curpos)
      elif output[0] == "1":
        white.add(curpos)
        if curpos in black:
          black.remove(curpos)
      if output[1] == "0":
        curdir.rotate(1)
      elif output[1] == "1":
        curdir.rotate(-1)
      curpos = (curpos[0] + directions[curdir[0]][0], curpos[1] + directions[curdir[0]][1])
      path.append(curpos)
      if curpos in white:
        input = "1"
      else:
        input = "0"
  return black, white

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  black, white = painter(code, "0")
  print("part 1: ", len(white) + len(black))
  
  #part 2
  black, white = painter(code, "1")
  plt.scatter(*zip(*white))
  plt.show()
  
  t1 = time.perf_counter()
  print(t1-t0)
  