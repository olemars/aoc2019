# -*- coding: utf-8 -*-
import time
from common import fetchinput
import string
from collections import deque
import heapq

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  m = [[x for x in l] for l in raw.split("\n")]
  char = None
  keys = {}
  doors = {}
  for y,x in ((y,x) for y in range(0,len(m)) for x in range(0,len(m[0]))):
    if m[y][x] == "@":
      char = (y,x)
    if m[y][x] in string.ascii_lowercase:
      keys[m[y][x]] = (y,x)
    if m[y][x] in string.ascii_uppercase:
      doors[m[y][x]] = (y,x)
      
  #part 1
  mov = [(1, 0), (-1, 0), (0, -1), (0, 1)]
  loc = char
  path = {char: None}
  targets = deque()
  for k in mov:
    t = (loc[0] + k[0], loc[1] + k[1])
    if t not in path:
      targets.append(t)
  while len(targets) > 0:
    target = targets.pop()
    path[target] = loc
    if m[target[0]][target[1]] not in "#" + string.ascii_uppercase:
      loc = target  
      for k in mov:
        t = (loc[0] + k[0], loc[1] + k[1])
        if t not in path:
          targets.appendleft(t)
  #part 2
    
  t1 = time.perf_counter()
  print(t1-t0)