# -*- coding: utf-8 -*-
import time
from common import fetchinput
import numpy as np
from matplotlib.pyplot import imshow

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  w = 25
  h = 6
  layers = np.array([int(c) for c in raw], dtype=np.int16).reshape(len(raw) // (w*h), h, w)
  
  #part 1
  lowestlayer = None
  lowestzeros = None
  for layer in layers:
    zeros = sum(layer.flatten() == 0)
    if lowestzeros is None or zeros < lowestzeros:
      lowestzeros = zeros
      lowestlayer = layer
  print("part 1: ", sum(lowestlayer.flatten() == 1) * sum(lowestlayer.flatten() == 2))
  
  #part 2
  img = layers[-1]
  for layer in layers[-2::-1]:
    img = np.where(layer < 2, layer, img)
  imshow(img)
  t1 = time.perf_counter()
  print(t1-t0)
  