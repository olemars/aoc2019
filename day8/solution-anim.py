# -*- coding: utf-8 -*-
import time
from common import fetchinput
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  w = 25
  h = 6
  layers = np.array([int(c) for c in raw], dtype=np.int16).reshape(len(raw) // (w*h), h, w)
  
  #part 1
  lowestlayer = None
  lowestzeros = None
  for layer in layers:
    zeros = sum(layer.flatten() == 0)
    if lowestzeros is None or zeros < lowestzeros:
      lowestzeros = zeros
      lowestlayer = layer
  print("part 1: ", sum(lowestlayer.flatten() == 1) * sum(lowestlayer.flatten() == 2))
  
  #part 2
  fig = plt.figure()

  metadata = dict(title='Day 8 anim', artist='me',
                comment='hackjob')
  writer = animation.ImageMagickWriter(fps=10, metadata=metadata)
  with writer.saving(fig, "day8.gif", 120):
    img = layers[-1]
    for layer in layers[-2::-1]:
      img = np.where(layer < 2, layer, img)
      im = plt.imshow(img, cmap="binary")
      writer.grab_frame()
    for i in range(20):
      writer.grab_frame()
    
  plt.imshow(img, cmap="binary")
  t1 = time.perf_counter()
  print(t1-t0)
  