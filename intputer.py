# -*- coding: utf-8 -*-

class Intputer:
  def __init__(self, program, stdout = True):
    self.program = []
    self.backup = []
    if program is not None:
      self.program = program
      self.backup = program.copy()
      
    self.iptr = 0
    self.relptr = 0
    self.exitcode = 0
    self.stdout = stdout
    self.output = []
    
    self.opcodes = {1: self.add, 
                    2: self.mul, 
                    3: self.cin, 
                    4: self.cout,
                    5: self.jit,
                    6: self.jif,
                    7: self.lt,
                    8: self.eq,
                    9: self.rel,
                    99: self.end}
         
  def add(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    c = self.getpos(self.iptr + 3, mode[2])
    self.program[c] = str(int(self.program[a]) + int(self.program[b]))
    self.iptr += 4
    
  def mul(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    c = self.getpos(self.iptr + 3, mode[2])
    self.program[c] = str(int(self.program[a]) * int(self.program[b]))
    self.iptr += 4
    
  def cin(self, mode):
    if len(self.stdin) > 0:
      c = self.getpos(self.iptr + 1, mode[0])
      self.program[c] = str(self.stdin.pop())
    else:
      self.exitcode = 1 #halt and wait for input
      return
    self.iptr += 2
      
  def cout(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    self.output.append(self.program[a])
    if self.stdout:
      print(self.program[a])
    self.iptr += 2
    
  def jit(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    if int(self.program[a]) != 0:
      self.iptr = int(self.program[b])
    else:
      self.iptr += 3
    
  def jif(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    if int(self.program[a]) == 0:
      self.iptr = int(self.program[b])
    else:
      self.iptr += 3
    
  def lt(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    c = self.getpos(self.iptr + 3, mode[2])
    self.program[c] = str(int(int(self.program[a]) < int(self.program[b])))
    self.iptr += 4
  
  def eq(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    b = self.getpos(self.iptr + 2, mode[1])
    c = self.getpos(self.iptr + 3, mode[2])
    self.program[c] = str(int(int(self.program[a]) == int(self.program[b])))
    self.iptr += 4
    
  def rel(self, mode):
    a = self.getpos(self.iptr + 1, mode[0])
    self.relptr += int(self.program[a])
    self.iptr += 2
    
  def end(self, mode):
    self.exitcode = -1
    
  def abort(self, error):
    print(error)
    self.exitcode = 2
    
  def getpos(self, pos, mode):
    newpos = 0
    if mode == "0":
      newpos = int(self.program[pos])
    elif mode == "1":
      newpos = pos
    elif mode == "2":
      newpos = self.relptr + int(self.program[pos])
    else:
      self.abort("invalid paramter mode: " + mode)
    if len(self.program) <= newpos:
      self.program.extend(["0"] * (1 + newpos - len(self.program)))
    return newpos
    
  def parsemode(self, v):
    return [s for s in v] + ["0"] * (max(0, 3 - len(v)))
  
  def reset(self):
    self.program = self.backup.copy()
    self.iptr = 0
    self.relptr = 0
    self.exitcode = 0
    self.output = []
            
  def run(self, stdin, clr = False):
    self.exitcode = 0
    self.stdin = []
    if stdin is not None:
      self.stdin = [*reversed(stdin)]
    if clr:
      self.output = []
      
    while 0 <= self.iptr < len(self.program) and self.exitcode == 0:
      #print(self.iptr, self.program[self.iptr])
      instr = self.program[self.iptr]
      opcode = int(instr[-2:])
      if opcode in self.opcodes:
        self.opcodes[opcode](self.parsemode(instr[-3::-1]))
      else:
        self.abort("invalid opcode: " + str(opcode))
    return self.output, self.exitcode