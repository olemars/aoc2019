# -*- coding: utf-8 -*-
import time
from common import fetchinput
from itertools import permutations
from intputer import Intputer

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  maxsignal1 = 0
  for s in permutations("01234"):
    output1 = [0]
    exitcode1 = 0
    while exitcode1 not in [-1, 2]:
      for c in s:
        lastout = output1[-1]
        puter = Intputer(code.copy(), False)
        output1, exitcode1 = puter.run([c, str(lastout)])
    if len(output1) > 0 and int(output1[-1]) > maxsignal1:
      maxsignal1 = int(output1[-1])
  print("part 1: ", maxsignal1)
  
  #part 2
  maxsignal2 = 0    
  for s in permutations("56789"):
    puters = [Intputer(code.copy(), False) for i in range(5)]
    output2 = [0]
    exitcode2 = 0
    
    #first run through initializes the phase
    for i in range(len(s)):
      c = s[i]
      lastout = output2[-1]
      output2, exitcode2 = puters[i].run([c, str(lastout)])
      if exitcode2 == 2:
        print("error: ", i)
        
    while exitcode2 not in [-1, 2]:
      for i in range(len(s)):
        lastout = output2[-1]
        output2, exitcode2 = puters[i].run([str(lastout)])
        if exitcode2 == 2:
          print("error: ", i)
    if len(output2) > 0 and int(output2[-1]) > maxsignal2:
      maxsignal2 = int(output2[-1])
  print("part 2: ", maxsignal2)
    
  t1 = time.perf_counter()
  print(t1-t0)
  