# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  x = 50
  y = 50
  #out = [["." for i in range(x)] for j in range(y)]
  output =  []
  total = 0
  puter = Intputer(code.copy(), False)
  j = 0
  sx = 0
  ex = 0
  for j in range(y):
    last = "0"
    i = sx
    while True:
      exitcode = 0
      puter.reset()
      output, exitcode = puter.run([i,j], True)
      if output[0] == "1" and last == "0":
        sx = i
        i = max(sx + 1, ex - 1)
        last = output[0]
        continue
      if output[0] == "0" and last == "1":
        ex = i
        break
      if output[0] == "0" and i > 50:
        ex = 0
        break
      #out[j][i] = "#" if output[0] == "1" else "."
      last = output[0]
      i += 1
    total += max(0, ex - sx)
#  for l in out:
#    print("".join(l))
  print("part 1: ", total)
  
  #part 2
  puter = Intputer(code.copy(), False)
  j = first = 50
  last = 20000
  starttop = startbottom = sx
  endtop = endbottom = ex
  while True:
    i = starttop-1
    lastout = "0"
    while True:
      puter.reset()
      output,_ = puter.run([i,j], True)
      if output[0] == "1" and lastout == "0":
        starttop = i
        i = max(starttop + 1, endtop - 1)
        lastout = output[0]
        continue
      if output[0] == "0" and lastout == "1":
        endtop = i
        break
      i += 1
      lastout = output[0]
    i = startbottom-1
    lastout = "0"
    while True:
      puter.reset()
      output,_ = puter.run([i,j+99], True)
      if output[0] == "1" and lastout == "0":
        startbottom = i
        i = max(startbottom + 1, endbottom - 1)
        lastout = output[0]
        continue
      if output[0] == "0" and lastout == "1":
        endbottom = i
        break
      i += 1
      lastout = output[0]
    if endtop - startbottom == 100:
      break
    elif endbottom - startbottom < 100:
      j += 99
    else:
      j += 1
    
  print("part 2: ", (startbottom * 10000) + j)   
  
  t1 = time.perf_counter()
  print(t1-t0)
  