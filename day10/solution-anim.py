# -*- coding: utf-8 -*-
import time
from common import fetchinput
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import math

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  roidmap = np.array([[c for c in y] for y in raw.split("\n")])
  roids = set(tuple(r) for r in np.argwhere(roidmap == "#"))
  
  fig = plt.figure()
  metadata = dict(title='Day 10 anim', artist='me',
                comment='hackjob')
  writer = animation.ImageMagickWriter(fps=10, metadata=metadata)
  
  #part 1
  bestloc = None
  largestcount = 0
  largestsect = None
  for s in roids:
    sect = {}
    for t in roids:
      x1, y1 = s
      x2, y2 = t
      if x1 != x2 or y1 != y2:
        diff = (x2-x1, y2-y1)
        dist = abs(diff[0]) + abs(diff[1])
        devi = -math.atan2(diff[1]/dist, diff[0]/dist)
        #devi = -np.angle(complex(*tuple(diff / dist)), deg=True)
        if devi not in sect:
          sect[devi] = [(dist, t)]
        else:
          sect[devi].append((dist, t))
    if largestcount < len(sect):
      largestcount = len(sect)
      largestsect = sect
      bestloc = s
      
  print("part 1: ", *reversed(bestloc), largestcount)   
  
  #part 2
  for x in largestsect.values():
    x.sort()
    x.reverse()
  vapor = 0
  lastvapor = None
  with writer.saving(fig, "day10.gif", 120):
    while vapor < 200 and len(largestsect) > 0:
      for x in sorted(largestsect):
        if len(largestsect) > 0:
          lastvapor = largestsect[x].pop()
          plt.cla()
          plt.ylim(0, roidmap.shape[0])
          plt.xlim(0, roidmap.shape[1])
          plt.scatter(*reversed([*zip(*roids)]))
          plt.scatter(*reversed(bestloc))
          plt.scatter(*reversed(lastvapor[1]), color="red")
          plt.plot(*reversed([*zip(bestloc, lastvapor[1])]), color="red")
          plt.gca().invert_yaxis()
          writer.grab_frame()
          roids.remove(lastvapor[1])
          vapor += 1
          if vapor == 200:
            break
    for i in range(20):
      writer.grab_frame()
  print("part 2: ", lastvapor[1][0] + lastvapor[1][1] * 100)
  
  t1 = time.perf_counter()
  print(t1-t0)
  