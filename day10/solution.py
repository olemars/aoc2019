# -*- coding: utf-8 -*-
import time
from common import fetchinput
import numpy as np
import math

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  roidmap = np.array([[c for c in y] for y in raw.split("\n")])
  roids = [tuple(r) for r in np.argwhere(roidmap == "#")]
  
  #part 1
  bestloc = None
  largestcount = 0
  largestsect = None
  for s in roids:
    sect = {}
    for t in roids:
      x1, y1 = s
      x2, y2 = t
      if x1 != x2 or y1 != y2:
        diff = (x2-x1, y2-y1)
        dist = abs(diff[0]) + abs(diff[1])
        devi = -math.atan2(diff[1]/dist, diff[0]/dist)
        #devi = -np.angle(complex(*tuple(diff / dist)), deg=True)
        if devi not in sect:
          sect[devi] = [(dist, t)]
        else:
          sect[devi].append((dist, t))
    if largestcount < len(sect):
      largestcount = len(sect)
      largestsect = sect
      bestloc = s
      
  print("part 1: ", *reversed(bestloc), largestcount)   
                       
  #part 2
  for x in largestsect.values():
    x.sort()
    x.reverse()
  vapor = 0
  lastvapor = None
  while vapor < 200 and len(largestsect) > 0:
    for x in sorted(largestsect):
      lastvapor = largestsect[x].pop()
      if len(largestsect[x]) == 0:
        largestsect.pop(x)
      vapor += 1
      if vapor == 200:
        break
  print("part 2: ", lastvapor[1][0] + lastvapor[1][1] * 100)
  
  t1 = time.perf_counter()
  print(t1-t0)
  