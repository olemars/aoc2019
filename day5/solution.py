# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 
  puter = Intputer(code.copy(), False)
  output1, exitcode1 = puter.run(["1"])
  print("part 1: ", output1[-1])
  
  #part 2
  puter = Intputer(code.copy(), False)
  output2, exitcode2 = puter.run(["5"])
  print("part 2: ", output2[-1])
  
  t1 = time.perf_counter()
  print(t1-t0)
  