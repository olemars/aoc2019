# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer
from collections import deque

def checkneighbor(m, point):
  i, j = point
  if 0 <= i < len(m) and 0 <= j < len(m[0]) and m[i][j] == "#":
    return True
  return False

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  puter = Intputer(code.copy(), False)
  exitcode = 0
  output = []
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run([], False)
  view = "".join([chr(int(c)) for c in output])
  scaffoldmap = [[x for x in l] for l in view.strip().split("\n")]
  
  alignmentparams = []
  for i,j in ((i,j) for i in range(1,len(scaffoldmap)-1) for j in range(1,len(scaffoldmap[0])-1)):
    if scaffoldmap[i][j] == "#" and \
       scaffoldmap[i-1][j] == "#" and \
       scaffoldmap[i+1][j] == "#" and \
       scaffoldmap[i][j-1] == "#" and \
       scaffoldmap[i][j+1] == "#":
      alignmentparams.append(i*j)
  print("part 1: ", sum(alignmentparams))
  
  #part 2
  start = None
  for i,j in ((i,j) for i in range(0,len(scaffoldmap)) for j in range(0,len(scaffoldmap[0]))):
    if scaffoldmap[i][j] in "^<>v":
      start = (i,j)
      break
 
  path = []
  curpos = start
  lastpos = None
  curdir = deque([(-1,0), (0,1), (1, 0), (0,-1)])
  steps = 0
  while True:
    target = curdir[0][0] + curpos[0], curdir[0][1] + curpos[1]
    if checkneighbor(scaffoldmap, target):
      curpos = target
      steps += 1
      continue
    curdir.rotate(-1)
    target = curdir[0][0] + curpos[0], curdir[0][1] + curpos[1]
    if checkneighbor(scaffoldmap, target):
      if steps > 0:
        path.append(str(steps))
        steps = 0
      path.append("R")
      continue
    curdir.rotate(2)
    target = curdir[0][0] + curpos[0], curdir[0][1] + curpos[1]
    if checkneighbor(scaffoldmap, target):
      if steps > 0:
        path.append(str(steps))
        steps = 0
      path.append("L")
      continue
    if steps > 0:
      path.append(str(steps))
    break
  
  ps = ",".join(path) + ","
  print(ps)
  markers = deque(["A", "B", "C"])
  blocks = []
  i = j = k = len(ps)
  count = 0
  while i >= 0 and ("R" in ps or "L" in ps):
    i -= 1
    block = ps[i:j]
    block.strip(",")
    count = len(block)
    if ps[i] in "ABC":
      if abs(j-i) > 1:
        block = ps[i+1:j]
        block = block.strip(",")
        print(block)
        blocks.append(block)
        ps = ps.replace(ps[i+1:j], markers.pop())
        i = j = k = len(ps)
        count = 0
      j = i
    elif ps[i] in "RL":
      if ps[i:j] in ps[:i] and (count <= 20 or len(blocks) == 2):
        k = i
      else:
        block = ps[k:j]
        block = block.strip(",")
        print(block)
        blocks.append(block)
        ps = ps.replace(ps[k:j], markers.popleft())
        i = j = k = len(ps)
        count = 0
    if len(markers) == 0:
      break
  print(ps)
  
  puter = Intputer(code.copy(), False)
  puter.program[0] = "2"
  exitcode = 0
  #ps = "BAABCBCBCA"
  movcom = [str(ord(c)) for c in ",".join([c for c in ps])]
  movcom.append(str(ord("\n")))
  #blocks = ["R,6,L,10,R,8", "R,6,R,6,R,8,L,10,L,4", "L,4,L,12,R,6,L,10"]
  A,B,C = [[str(ord(c)) for c in block] for block in blocks]
  A.append(str(ord("\n")))
  B.append(str(ord("\n")))
  C.append(str(ord("\n")))
  cam = [str(ord("n"))]
  cam.append(str(ord("\n")))
  
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run(movcom + A + B + C + cam, False)
    #view = "".join([chr(int(c)) for c in output])
    #print(view)
  print("part 2: ", output[-1])
  t1 = time.perf_counter()
  print(t1-t0)
  
  