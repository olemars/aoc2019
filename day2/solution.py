# -*- coding: utf-8 -*-
from common import fetchinput

def checkbounds(a, b, c, intcode):
    bound = len(intcode)
    if 0 <= a < bound and \
        0 <= b < bound and \
        0 <= c < bound and \
        0 <= intcode[a] < bound and \
        0 <= intcode[b] < bound and \
        0 <= intcode[c] < bound:
        return True
    return False

def add(pos, intcode):
    a = pos + 1
    b = pos + 2
    c = pos + 3
    if checkbounds(a, b, c, intcode):
        intcode[intcode[c]] = intcode[intcode[a]] + intcode[intcode[b]]
    pos += 4
    return pos
    
def mul(pos, intcode):
    a = pos + 1
    b = pos + 2
    c = pos + 3
    if checkbounds(a, b, c, intcode):
        intcode[intcode[c]] = intcode[intcode[a]] * intcode[intcode[b]]
    pos += 4
    return pos
    
def run(intcode):
    iptr = 0
    while 0 <= iptr <= len(intcode):
        if intcode[iptr] == 99:
            return intcode[0]
        elif intcode[iptr] == 1:
            iptr = add(iptr, intcode)
        elif intcode[iptr] == 2:
            iptr = mul(iptr, intcode)
    return intcode[0]
        

fetchinput()
with open("input.txt") as infile:
    raw = infile.read().strip()
    original = [int(x) for x in raw.split(",")]
    
    #part 1
    program = original.copy()
    program[1] = 12
    program[2] = 2
    print(run(program))
    
    #part 2    
    for i, j in ((i,j) for i in range(100) for j in range(100)):
            program = original.copy()
            program[1] = i
            program[2] = j
            if run(program) == 19690720:
                print(i, j, (100 * i) + j)
                break
            