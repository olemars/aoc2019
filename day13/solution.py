# -*- coding: utf-8 -*-
import time
from common import fetchinput
from intputer import Intputer
import numpy as np

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  code = raw.split(",")
  
  #part 1
  puter = Intputer(code.copy(), False)
  exitcode = 0
  output = []
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run([], False)
  blocks = sum([x=="2" for x in output[2::3] ])
  print("part 1 : ", blocks)
  
 
  #part 2
  board = np.zeros((max([int(y) for y in output[1::3]]) + 1, max([int(x) for x in output[0::3]]) + 1))  
  for t in zip([int(x) for x in output[0::3]], [int(y) for y in output[1::3]], [int(v) for v in output[2::3]]):
    x,y,v = t
    if x != -1:
      board[y,x] = v
      
  puter = Intputer(code.copy(), False)
  puter.program[0] = "2"
  exitcode = 0
  output = []
  blocks = 0
  score = 0
  paddle = 0
  while exitcode not in [-1, 2]:
    output, exitcode = puter.run([str(paddle)], True)
    for t in zip([int(x) for x in output[0::3]], [int(y) for y in output[1::3]], [int(v) for v in output[2::3]]):
      x,y,v = t
      if x != -1:
        board[y,x] = v
    blocks = np.sum(board == 2)
    if blocks != 0:
      ballx = np.argwhere(board == 4)[0][1]
      paddlex = np.argwhere(board == 3)[0][1]
      paddle = 0 if paddlex == ballx else np.sign(ballx - paddlex)
    if "-1" in output[0::3]:
      score = int(output[output.index("-1") + 2])
      if blocks == 0:
        break
  print("part 2 : ", score)
  
  t1 = time.perf_counter()
  print(t1-t0)
  