# -*- coding: utf-8 -*-
import time
from common import fetchinput

class Node:
  def __init__(self, name):
    self.name = name
    self.parent = None
    self.depth = None
      
  def setParent(self, node):
      self.parent = node
      
  def getDepth(self):
    if self.depth is None:
      self.depth = 1 + self.parent.getDepth() if self.parent is not None else 0
    return self.depth
  
  def getTransfers(self):
    transfers = set()
    node = self
    while node.parent != None:
      transfers.add(node.parent.name)
      node = node.parent
    return transfers

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  links = [tuple(y for y in x.split(")")) for x in raw.split("\n")]
  nodeMap = {}
  for a, b in links:
    parent = Node(a) if a not in nodeMap else nodeMap[a]
    child = Node(b) if b not in nodeMap else nodeMap[b]
    child.setParent(parent)
    nodeMap[a] = parent
    nodeMap[b] = child
  
  #part 1
  orbitsum = sum([node.getDepth() for node in nodeMap.values()])
  print("part 1: ", orbitsum)
      
  #part 2
  you = nodeMap["YOU"].getTransfers()
  santa = nodeMap["SAN"].getTransfers()
  transfers = len(you ^ santa)
  print("part 2: ", transfers)
  
  t1 = time.perf_counter()
  print(t1-t0)
  