# -*- coding: utf-8 -*-
import time
from common import fetchinput
from math import ceil
from collections import defaultdict, deque

class Recipe:
  def __init__(self, p):
    l, r = p.split(" => ")
    result = r.split(" ")
    self.q, self.name = int(result[0]), result[1]
    self.reactants = {}
    self.needed = 0
    self.batches = 0
    for x in [s.split(" ") for s in l.split(", ")]:
      self.reactants[x[1]] = int(x[0])
      
fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  recipes = {}
  for l in raw.split("\n"):
    r = Recipe(l)
    recipes[r.name] = r
  recipes["ORE"] = Recipe("1 ORE => 1 ORE")
  recipes["ORE"].reactants = {}
  
  #part 1
  steps = deque([("FUEL", 1)])
  while len(steps) > 0:
    step = steps.popleft()
    recipe = recipes[step[0]]
    recipe.needed += step[1]
    #print(step, recipe.needed, recipe.q)
    neededbatches = max(0, ceil(recipe.needed / recipe.q))
    if neededbatches > recipe.batches:
      for item in recipe.reactants.items():
        steps.append((item[0], item[1] * (neededbatches - recipe.batches)))
    recipe.batches = neededbatches
  print("part 1: ", recipes["ORE"].needed)
      
  #part 2
  targ = recipes["ORE"].needed / 2
  first = 1
  last = 100000000
  fuel = first
  while True:
    for recipe in recipes.values():
      recipe.needed = 0
      recipe.batches = 0
    steps = deque([("FUEL", fuel)])
    while len(steps) > 0:
      step = steps.popleft()
      recipe = recipes[step[0]]
      recipe.needed += step[1]
      neededbatches = max(0, ceil(recipe.needed / recipe.q))
      if neededbatches > recipe.batches:
        for item in recipe.reactants.items():
          steps.append((item[0], item[1] * (neededbatches - recipe.batches)))
      recipe.batches = neededbatches
    if 1000000000000 - targ < recipes["ORE"].needed < 1000000000000:
      break
    elif recipes["ORE"].needed > 1000000000000:
      last = fuel
      fuel = (first + fuel) // 2
    elif 1000000000000 - targ > recipes["ORE"].needed:
      first = fuel
      fuel = (last + fuel) // 2
      
  print("part 2: ", fuel) 
    
  t1 = time.perf_counter()
  print(t1-t0)
  