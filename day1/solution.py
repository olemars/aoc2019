# -*- coding: utf-8 -*-

with open("input.txt") as infile:
    raw = infile.read().strip()
    modules = [int(x) for x in raw.split()]
    
    #part 1
    fuelreq = 0
    for x in modules:
        fuelreq += max(0, int(x/3) - 2)
    print("part 1: ", fuelreq)
    
    #part 2
    fuelreq = 0
    for x in modules:
        while x > 0:
            x = max(0, int(x/3) - 2)
            fuelreq += x
    print("part 2: ", fuelreq)
