# -*- coding: utf-8 -*-
import time
from common import fetchinput
from math import gcd

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt

def sign(v):
  if v > 0:
    return 1
  if v < 0:
    return -1
  return 0 

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  moondefs = [l.strip("<>").split(", ") for l in raw.split("\n")]
  moons = [[int(c.split("=")[1]) for c in l] for l in moondefs]
  coords = tuple(map(list, zip(*moons)))
  vels = tuple([0 for i in range(4)] for j in range(3))
  total = []
  x = []
  y = []
  z = []
  #part 1
  for s in range(1000):
    for pa,va in zip(coords, vels):
      for i in range(len(va)):
        va[i] += sum([sign(p - pa[i]) for p in pa])
      for i in range(len(pa)):
        pa[i] += va[i]
    x.append(coords[0][0])
    y.append(coords[1][0])
    z.append(coords[2][0])
    pot = [sum([abs(a) for a in m]) for m in tuple(map(list, zip(*coords)))]
    kin = [sum([abs(a) for a in m]) for m in tuple(map(list, zip(*vels)))]
    tot = 0
    for p, k in zip(pot, kin):
      tot += p*k
    total.append(tot)
  print("Part 1: ", total[-1])
  fig = plt.figure()
  ax = plt.axes(projection='3d')
  ax.plot3D(x,y,z)
  
  #part 2
  intervals = []
  coords = tuple(map(list, zip(*moons)))
  vels = tuple([0 for i in range(4)] for j in range(3))
  for pa,va in zip(coords, vels):
    coordcheck, velcheck = tuple(pa), tuple(va)
    interval = None
    check = None
    s = 0
    while interval is None:
      s += 1
      for i in range(len(va)):
        va[i] += sum([sign(p - pa[i]) for p in pa])
      for i in range(len(pa)):
        pa[i] += va[i]
      if tuple(pa) == coordcheck and tuple(va) == velcheck:
        if interval is None:
          interval = s
    intervals.append(interval)
  a,b,c = intervals
  loop = int((a/gcd(a,b)) * (b/gcd(b,c)) * c)
  print("Part 2: ", loop)
  t1 = time.perf_counter()
  print(t1-t0)
  